import cv2
import pyautogui
import numpy as np
import os

while True:
    gray = cv2.cvtColor(np.array(pyautogui.screenshot()), cv2.COLOR_RGB2GRAY)
    gray = cv2.GaussianBlur(gray, (55, 55), 0)
    maxVal = cv2.minMaxLoc(gray)[1]
    newBrightness = str(2.0 - (maxVal / 256.0))
    os.system("xrandr --output LVDS-1 --brightness " + newBrightness)
