### Linux Brightness Maximizer ###

This is a small script written in Python 3 that attempts to maximize the possible brightness on a user's screen.
This script is especially useful for those with dim displays.

Essentially what this script does is increase the intensity of pixels so that the brightest pixel on the user's screen
is always 100% white.

### How do I use it? ###
I've only tested this script on my own machine, which is running Fedora 28 KDE, but
it should work on any Linux machine that supports Xrandr.

Make sure to install python3, opencv, numpy, xlib, pyautogui and scrot.
Also make sure to change **LVDS-1** in the script to the name of your own display.
You can get the name of your display by typing the following command in bash:
    
    xrandr -q | grep connected
